'use strict';
/*
    TIPS:
	you can use Javascripts Fetch method to get the data - https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
	i.e. - https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

	fetch('http://example.com/movies.json')
	.then(response => response.json())
	.then(data => console.log(data));
	
	IFFE explained - https://developer.mozilla.org/en-US/docs/Glossary/IIFE
	
	Grabbing elements you can create use
	const tracks = document.querySelectorAll('.tracks li');
	tracks.forEach(function(el, i){
	    ... my code goes here...
	});

	or alternatively use newer style arrow functions :-)

	Remember Google is your frriend.

*/