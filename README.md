# README #

## 1) (Using Php, Javascript, Python whichever language you prefer)

A. Use a (constructor function or class) to implement a Boat class. A boat has a type and a speed property. The speed property is the current speed of the boat in knots;
B. Implement a 'windup' method that will increase the boat's speed by 10, and log the new speed to the console or print on the page;
C. Implement a 'winddown' method that will decrease the boat's speed by 5, and log the new speed to the console or print on the page;
D. Create two boat objects and experiment with calling 'windup' and 'winddown' multiple times on each of them and log them to the console or print on the page.

DATA BOAT 1: 'Schooner' going at 25 knots
DATA BOAT 2: 'Clipper' going at 15 knots



## 2) Votes & Views
Everyone who loves a track likes to vote it up! 
Currently every time any user (logged in or not) lands on the page it counts as a view, but a user can only vote for a track when they are logged in.
In a few sentences describe why this is an issue and explain your own solution for this.



## 3) Workflow diagram
Put together a very simple flow diagram to show how users would be able to upload and sell their own tracks so that other users could purchase them.
Currently users can enter their own PayPal details to accept payment directly.
Draw how you would plan the upload process, consider how would the tracks be stored in a way that could not be downloaded until paid for? How would they be stored in a protected way?
And after successful checkout how would the user be able to gain access to the track to download it?



## 4) Output some data from our website using the attached template:
Build a simple HTML output using a list (Using Php, Javascript/JQuery, Python) to output data by using our API’s – 
https://jeeni.com/wp-json/Jeeni_Plugin/v2/videosby/98

a)	Loop through the tracks to display the following information by replacing the HTML placeholders {{ID}}:
                                ID
                                track title (post_title)
                                excerpt

b)	For each track in the loop then call this API to retrieve additional data about the track and output using the ID:
                https://jeeni.com/wp-json/Jeeni_Plugin/v2/trackdata/{{ID}} - IE https://jeeni.com/wp-json/Jeeni_Plugin/v2/trackdata/21900

                                image (thumbnail)
                                link to the track on the website (permalink)
                                If data is not defined or empty, simply output "no data"

Try to use Classes and Methods if you can try to structure your code in a nice package.
Bonus - If you are using Javascript to do this, then wrap the code in an IFFE to protect the scope alternatively make your PHP/Ruby/Python class protected from an outside scope.

StackOverflow or other Googling is always allowed! In fact, it’s encouraged.

GOOD LUCK 😀
